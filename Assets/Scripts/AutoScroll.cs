using UnityEngine;

public class AutoScroll : MonoBehaviour
{
    public float scrollSpeed = 2.0f; // Velocidad de desplazamiento

    void Update()
    {
        // Mueve el objeto en la dirección del eje X
        transform.Translate(Vector3.right * scrollSpeed * Time.deltaTime);
    }
}
