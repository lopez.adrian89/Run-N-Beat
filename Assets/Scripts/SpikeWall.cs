using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeWall : MonoBehaviour
{
    
    public GameObject startObject; // Referencia al objeto "Start" en la escena

    private void Start()
    {
        // Desactiva el objeto "Start" al inicio
        startObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            foreach (ContactPoint2D punto in other.contacts)
            {
    
                other.gameObject.GetComponent<PlayerMovement>().Muerte();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            // Activa el objeto "Start" cuando el jugador lo toca
            startObject.SetActive(true);
        }
    }
}
